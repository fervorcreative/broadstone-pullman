<?php

$id = get_the_ID();

if (have_rows( 'sections', $id )) :
  while ( have_rows( 'sections', $id ) ) : the_row();
    get_template_part( 'partials/' . get_row_layout() );
  endwhile;
endif; ?>
