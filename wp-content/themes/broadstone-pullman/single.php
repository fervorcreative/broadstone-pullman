<?php
/**
 * The template for displaying all single posts
 */

get_header(); ?>

<main role="main">

	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'partials/content', 'single' ); ?>

		<?php the_post_navigation(); ?>

		<?php
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
		?>

	<?php endwhile; ?>

</main>

<?php get_footer(); ?>
