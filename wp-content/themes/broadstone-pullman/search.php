<?php
/**
 * The template for displaying Search Results pages
 */

get_header(); ?>

<main role="main">

	<section class="page page--search">

		<?php if ( have_posts() ) : ?>

			<header class="page__header">
				<h1><?php printf( __( 'Search Results for: %s' ), '<span>' . get_search_query() . '</span>' ); ?>
			</header>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'partials/content', 'search' ); ?>

			<?php endwhile; ?>

			<?php the_posts_navigation(); ?>

		<?php else : ?>

			<?php get_template_part( 'partials/content', 'none' ); ?>

		<?php endif; ?>

	</section><!-- .search -->

</main>

<?php get_footer(); ?>
