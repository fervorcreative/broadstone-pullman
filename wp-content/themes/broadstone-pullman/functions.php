<?php

// Theme supports

add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

// Crop images for gallery slider thumbnails
add_image_size( 'grid-crop', 456, 456, true );

// Post type supports
add_post_type_support( 'page', 'excerpt' );

// Register nav menus

register_nav_menu( 'header', 'Header' );
register_nav_menu( 'header_offcanvas', 'Header (Hidden)' );

function pullman_body_classes($classes) {
  global $post;
  $classes[] = $post->post_name;
    return $classes;
}
add_filter('body_class', 'pullman_body_classes');

// Enqueue CSS styles

function init_enqueue_css() {
  wp_enqueue_style( 'dashicons' );
  wp_enqueue_style( 'slick-styles', get_template_directory_uri() . '/dist/slick.css');
  wp_enqueue_style( 'theme', get_template_directory_uri() . '/dist/app.css', array(), null);
}
add_action( 'wp_enqueue_scripts', 'init_enqueue_css' );

// Enqueue Admin CSS styles

function init_enqueue_admin_css() {
	wp_enqueue_style( 'admin-styles', get_template_directory_uri() . '/admin/style.css', array(), '', null);
  }
  add_action( 'admin_enqueue_scripts', 'init_enqueue_admin_css');

// Enqueue Javascript
function init_enqueue_js() {
	if ( ! is_admin() ) {
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', false, false, false );
		wp_enqueue_script( 'jquery' );
	}
	wp_enqueue_script( 'tabsjs' , get_stylesheet_directory_uri()  . '/assets/js/vendor/tabs.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'google-maps-api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA9ju5hO9ANZURJ3E-RChL_hx9aj3I6nFU', array('jquery'), null, false);
	wp_enqueue_script( 'lottie', 'https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.5.0/lottie.js', array('jquery'), '', false );
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/vendor/modernizr-custom.js', array('jquery'), '3.6.0', false );
	wp_enqueue_script( 'app', get_template_directory_uri() . '/dist/app.js', array( 'jquery' ), null, true );

	// Localize template URL for usage in JS
	$data = array(
		'template_dir' => get_stylesheet_directory_uri(),
		'home_url' => home_url()
	);

	wp_localize_script( 'app', 'AppData', $data );
}
add_action( 'wp_enqueue_scripts', 'init_enqueue_js' );

// Register Locations CPT
function register_locations_cpt() {
  register_post_type( 'locations',
    array(
      'labels' => array(
        'name' => __( 'Locations' ),
        'singular_name' => __( 'Location' )
      ),
    'public' => true,
    'has_archive' => true,
	  'menu_icon' => 'dashicons-location',
	  'taxonomies' => array('category'),
	  'supports' => array('title', 'custom-fields'),
    'rewrite' => array('slug' => 'locations'),
    )
  );
}
add_action( 'init', 'register_locations_cpt' );

// Custom labels for ACF sections

function my_layout_title($title, $field, $layout, $i) {
	if($value = get_sub_field('layout_title')) {
		return $value;
	} else {
		foreach($layout['sub_fields'] as $sub) {
			if($sub['name'] == 'layout_title') {
				$key = $sub['key'];
				if(array_key_exists($i, $field['value']) && $value = $field['value'][$i][$key])
					return $value;
			}
		}
	}
	return $title;
}
add_filter('acf/fields/flexible_content/layout_title', 'my_layout_title', 10, 4);

//Init Google Analytics

function init_google_analytics() { ?>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-83547238-16"></script>
	<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-83547238-16');
	</script>
<?php
}
add_action('wp_head', 'init_google_analytics');

//Some modifier classes for adding section padding through ACF fields

function padding_top_classes() {
$section_paddingtop = (!empty(get_sub_field('section_padding_top')) ? get_sub_field('section_padding_top') : '');
if (!empty($section_paddingtop)):
	if ($section_paddingtop == 'none'):
		echo ' nopadding-top';
	elseif ($section_paddingtop == 'small'):
		echo ' paddingtop-small';
	elseif ($section_paddingtop == 'medium'):
		echo ' paddingtop-medium';
	elseif ($section_paddingtop == 'large'):
		echo ' paddingtop-large';
	elseif ($section_paddingtop == 'xlarge'):
		echo ' paddingtop-xlarge';
	elseif ($section_paddingtop == 'xxlarge'):
		echo ' paddingtop-xxlarge';
	else:
		echo '" style="padding-top:'.$section_paddingtop.'px';
  endif;
endif;
} 

function padding_bottom_classes() {
$section_paddingbottom = (!empty(get_sub_field('section_padding_bottom')) ? get_sub_field('section_padding_bottom') : ''); 
if (!empty($section_paddingbottom)):
  if ($section_paddingbottom == 'none'):
    echo ' nopadding-bottom';
  elseif ($section_paddingbottom == 'small'):
    echo ' paddingbottom-small';
  elseif ($section_paddingbottom == 'medium'):
    echo ' paddingbottom-medium';
  elseif ($section_paddingbottom == 'large'):
    echo ' paddingbottom-large';
  elseif ($section_paddingbottom == 'xlarge'):
    echo ' paddingbottom-xlarge';
  elseif ($section_paddingbottom == 'xxlarge'):
    echo ' paddingbottom-xxlarge';
  else:
    echo '" style="padding-bottom:'.$section_paddingbottom.'px';
  endif;
endif;
}

// Additional theme includes
require_once( get_template_directory() . '/inc/functions-defaults.php');
require_once( get_template_directory() . '/inc/functions-helpers.php'); ?>
