// Initialize modules
const { src, dest, watch, series, parallel } = require('gulp');
const gulp = require('gulp');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const notify = require('gulp-notify');
const eslint = require('gulp-eslint');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const sassglob = require('gulp-sass-glob');
const postcss = require('gulp-postcss');
const cssnano = require('gulp-cssnano');
const autoprefixer = require('autoprefixer');
const rucksack = require('rucksack-css');
const sourcemaps = require('gulp-sourcemaps');

// Variables
const files = {
  sassPath: 'assets/sass/app.sass',
  sassFilesPath: 'assets/sass/**/*.sass',
  jsPath: 'assets/js/*.js'
}

const jsFiles = [
  'node_modules/slick-slider/slick/slick.min.js',
  'node_modules/aos/dist/aos.js',
  'node_modules/rellax/rellax.min.js',
  'node_modules/mixitup/dist/mixitup.min.js',
  'node_modules/magnific-popup/dist/jquery.magnific-popup.min.js',
  files.jsPath
]

// Alert
const alert = function(error) {
  notify.onError({
    title:    'Gulp',
    subtitle: 'Failure!',
    message:  'Error: <%= error.message %>',
    sound:    'Submarine',
    onLast:   true
  })(error);

  this.emit('end');
}

// Compile Sass to CSS
function sassTask() {
  return src(files.sassPath)
    .pipe(plumber({
      errorHandler: alert
    }))
    .pipe(sourcemaps.init())
    .pipe(sassglob())
    .pipe(sass())
    .pipe(postcss([
      autoprefixer(),
      rucksack()
    ]))
    .pipe(cssnano())
    .pipe(sourcemaps.write('.'))
    .pipe(dest('dist'))
    .pipe(notify({
      title:    'Gulp Task Complete',
      message:  'Sass compilation complete',
      sound:    'Submarine',
      onLast:   true
    }));
}

// Compile JavaScript
function jsTask() {
  return src(jsFiles)
    .pipe(plumber({
      errorHandler: alert
    }))
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError())
    .pipe(uglify())
    .pipe(concat('app.js'))
    .pipe(dest('dist'))
    .pipe(notify({
      title:   'Gulp Task Complete',
      message: 'JavaScript compilation complete',
      sound:   'Submarine',
      onLast:  true
    }));
}

// Watch Sass and JS Files
function watchTask() {
  watch([files.sassFilesPath], sassTask);
  watch([files.jsPath], jsTask);
}

// Compile both CSS and JS Files
const buildTask = parallel(sassTask, jsTask);

// Export Tasks
exports.build = buildTask;
exports.sass = series(sassTask);
exports.js = jsTask;
exports.watch = series(buildTask, watchTask);
exports.default = buildTask;