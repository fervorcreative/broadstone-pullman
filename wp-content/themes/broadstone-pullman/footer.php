  </main>

	<footer id="footer" class="section footer grid-container" role="contentinfo">
		<div class="grid-x align-center text-center align-middle">
			<div class="cell medium-6 large-6 footer__logo">
				<img src="/wp-content/uploads/2019/11/footer-logos.png" alt="alliance-residential" />
      </div>

      <div class="cell medium-6 large-6 footer__social">
        <a class="social" href="https://www.instagram.com/broadstonepullman/" target="_blank">Instagram</a> <span class="divider">|</span> <a class="social" href="https://business.facebook.com/BroadstonePullman/" target="_blank">Facebook</a> <span class="divider">|</span> <a class="social" href="https://www.youtube.com/channel/UCBs1eJz3oMx708BJdodAQfg/videos" target="_blaank">YouTube</a>
      </div> <!-- .cell --> 

      <div class="footer__links">
        <span class="footer__copyright">&copy;2019 Alliance Residential. All Rights Reserved.</span>
        <a href="https://www.liveatalliance.com/" target="_blank">Professionally Managed by Alliance Residential</a>
        <a href="https://www.liveatalliance.com/privacy-policy.aspx" target="_blank">Privacy Policy</a>
        <a href="https://www.liveatalliance.com/terms-and-conditions.aspx" target="_blank">Legal</a>
        <a href="https://www.liveatalliance.com/fair-housing.aspx" target="_blank">Fair Housing</a>
      </div> <!-- .footer__links -->
			</div> <!-- .row -->
		</div> <!-- .container -->
	</footer>

</div> <!-- .site -->

<?php wp_footer(); ?>

<script>
var loader = document.getElementsByClassName("bodymovin");
function loadBMAnimation(loader) {
 var animation = bodymovin.loadAnimation({
   container: loader,
   renderer: "svg",
   loop: false,
   autoplay: true,
   path: "/wp-content/themes/broadstone-pullman/animations/logo.json"
 });
}
for (var i = 0; i < loader.length; i++) {
 loadBMAnimation(loader[i]);
}
</script>

<script async src='//statrack.leaselabs.com/sifitag/ff63aac0-6d27-0136-4c42-067f653fa718'></script>

</body>
</html>
