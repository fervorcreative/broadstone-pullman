<?php

$hero_image = get_sub_field('hero_image');
$logo = get_sub_field('logo');
$logo_text_1 = get_sub_field('logo_text_1');
$logo_text_2 = get_sub_field('logo_text_2');
$logo_tagline = get_sub_field('logo_tagline');
$banner_headline = get_sub_field('banner_headline');
$banner_headline_mobile = get_sub_field('banner_headline_mobile');
$custom_label = get_sub_field('custom_label'); ?>

<section id="section<?= get_row_index(); ?>" class="section homepage-hero<?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>" data-aos="fade-up" data-aos-delay="300" style="<?= (!empty($hero_image['url']) ? 'background:url('.$hero_image['url'].') center center / cover no-repeat' : ''); ?>">
  <div class="grid-container">
    <div class="grid-x align-middle" data-aos="fade-up" data-aos-delay="500">
      <div class="cell small-12 medium-12 large-12">
        <div class="hero-content">
          <div class="bodymovin-wrapper animated fadeIn">
            <div id="bodymovin"></div>
          </div> <!-- .bodymovin -->
    
          <img class="animated fadeIn2" src="/wp-content/uploads/2019/11/broadstone.png" alt="Broadstone Pullman Logo" />
          <img class="animated fadeIn3" src="/wp-content/uploads/2019/11/pullman.png" alt="Broadstone Pullman Logo" />

          <p class="tagline animated fadeIn4"><?= $logo_tagline; ?></p>
        </div> <!-- .hero-content -->
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
    <p class="headline"><?= $banner_headline; ?></p>
    <p class="headline responsive"><?= $banner_headline_mobile; ?></p>
  </div> <!-- .grid-container -->
  <div class="lineDraw fadeInStarter"></div>
  <div class="lineDraw2 fadeInStarter"></div>
</section>