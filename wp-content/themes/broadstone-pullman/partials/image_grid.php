<?php

$expanded_grid = get_sub_field('expanded_grid'); 
$bg_color = get_sub_field('background_color'); 
$count = 0;
$images = get_sub_field('grid_images');

$images_lightbox = get_sub_field('lightbox_images');
$numberOfColumns = 4;
$bootstrapColWidth = 12 / $numberOfColumns;

$arrayChunks = array_chunk($images, $numberOfColumns) ?>

<section id="section<?= get_row_index(); ?>" class="section gallery-slider" style="<?= (!empty($bg_color) ? 'background-color:'.$bg_color : ''); ?>">
  <div class="grid-container full">
    <div class="grid-x align-center">
      <div class="cell large-12">
        <div class="image-slider-section relative">
          <?php if(!empty($images_lightbox)): ?>
            <div class="slide-wrapper">
              <div id="image-altanv-slider" class="image-slider" data-index="<?= get_row_index(); ?>">
                <?php foreach($images_lightbox as $image): ?>
                  <?php 
                      $slide_img = wp_get_attachment_image_src( $image['ID'], 'full'); ?>
                      <div class="image-slide" style="background-image: url(<?= $slide_img[0]; ?>); background-position: center center"></div>
                  <?php $count++; endforeach; ?>
              </div> <!-- #image-slider --> 
              <div class="slide-controls slide-controls">
                <div class="slide-counter"></div>
              </div> <!-- .slide-controls -->
            </div> <!-- .slide-wrapper -->
          <?php endif; ?>
        </div> <!-- .image-slider-section -->
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section> <!-- section -->