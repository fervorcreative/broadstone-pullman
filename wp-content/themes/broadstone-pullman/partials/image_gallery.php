<?php

  $expanded_grid = get_sub_field('expanded_grid'); ?>

  <section id="section<?= get_row_index(); ?>" class="section gallery-slider">
    <div class="grid-container full">
      <div class="grid-x align-center">
        <div class="cell large-12">
          <div class="image-slider-section relative">
            <div class="tab-wrapper">
              <?php 
                $slider_counter = 1;
                if (have_rows('gallery')): while(have_rows('gallery')): the_row(); 
                  $gallery_name = get_sub_field('gallery_name');
                  $gallery_slug = str_replace( ' ', '-', $gallery_name );
                  $gallery_slug = strtolower( $gallery_slug ); ?>

                  <div id="<?= $gallery_slug; ?>" class="tab-content slide-wrapper <?= ($slider_counter == 1 ? 'current-tab' : ''); ?>">
                    <div id="image-slider-<?= $gallery_slug; ?>" class="image-slider image-bspullman-slider" data-index="<?= $gallery_slug; ?>">
                      <?php $count=1; $images = get_sub_field('gallery_images'); foreach($images as $image):
                        $slide_img = wp_get_attachment_image_src( $image['ID'], 'full'); ?>
                        <div class="image-slide" style="background-image: url(<?= $slide_img[0]; ?>); background-position: center center"></div>
                      <?php $count++; endforeach; ?>
                    </div> <!-- #image-slider --> 

                    <div class="slide-controls slide-controls-<?= $gallery_slug; ?>">
                      <div class="slide-counter"></div>
                    </div> <!-- .slide-controls -->
                  </div> <!-- .slide-wrapper -->
                <?php $slider_counter++; endwhile; endif; ?>
            </div> <!-- .tab-wrapper -->
          </div> <!-- .image-slider-section -->
        </div> <!-- .cell -->
      </div> <!-- .grid-x --> 
    </div> <!-- .grid-container -->

    <div class="grid-container">
      <div class="grid-x">
        <div class="cell small-12 medium-12 large-12">

          <ul class="tabs menu">
              <?php 
                $slider_counter = 1;
                if (have_rows('gallery')): while( have_rows('gallery')) : the_row();
                  $gallery_name = get_sub_field('gallery_name');
                  $gallery_slug = str_replace( ' ', '-', $gallery_name );
                  $gallery_slug = strtolower( $gallery_slug ); ?>
                  <li <?php if($slider_counter == 1) echo 'class="current-tab"' ?>><a href="#<?php echo $gallery_slug; ?>"><?php echo $gallery_name; ?></a><span></span></li>
                <?php $slider_counter++; endwhile; endif; ?>
            </ul>
          
            <div class="tab-wrapper pager-wrapper">
              <?php 
                $j=0;
                while( have_rows('gallery') ) : the_row(); $j++;
                  $galname = get_sub_field( 'gallery_name');
                  $galslug = str_replace( ' ', '-', $galname );
                  $galslug = strtolower( $galslug ); ?>
                
                  <div id="<?= $galslug; ?>-pager" class="tab-content tab-pager <?php if($j == 1) echo 'current-tab' ?>">
                    <div class="image-grid photos" data-slider="slider-<?php echo $galslug; ?>">
                      <?php 
                        $images = get_sub_field( 'gallery_images' ); 
                        $s = 0;
                        foreach( $images as $image ) : $s++;?>
                          <a href="#" data-slide="<?php echo $s; ?>" style="background-image:url(<?= wp_get_attachment_image_url($image['ID'], 'grid-crop'); ?>)"></a>
                      <?php endforeach; ?>
                      </div>
                  </div>
              <?php endwhile; ?>
            </div>
        </div>
      </div>
    </div> <!-- .grid-container --> 
</section> <!-- section -->