<section id="section<?= get_row_index(); ?>" class="section grid-container full pre-footer" data-aos="fade-left" data-aos-delay="300">
  <div class="grid-x align-bottom text-center">
    <div class="cell flex-dir-column">
      <img class="section__logo" src="/wp-content/uploads/2019/11/logo.png" alt="broadstone-pullman" />
      <p class="section__copyright">6303 Frisco Square Blvd., Frisco, TX 75034</p>
    </div> <!-- .row -->
  </div> <!-- .container -->
</section>