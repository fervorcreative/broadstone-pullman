<?php
$content = get_sub_field( 'content' ); 
?>

<section id="section<?= get_row_index(); ?>" class="section grid-container standard-content<?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>">
  <div class="grid-x align-center">
    <div class="cell">
      <?= $content; ?>
    </div> <!-- .cell -->
  </div> <!-- .grid-x -->
</section>