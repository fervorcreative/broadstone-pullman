<?php 
  
  $background_image = get_sub_field('bg_image');
  $form_id = get_sub_field('form_id');
  $section_title = get_sub_field('section_title');
  $section_title_mobile = get_sub_field('section_title_mobile');
  $title = get_sub_field('title');
  $copy = get_sub_field('copy'); ?>

<section id="section<?= get_row_index(); ?>" class="section contact-form<?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>" data-aos="fade-up" data-aos-delay="300" style="<?= (!empty($background_image) ? 'background-image:url('.$background_image['url'].')' : ''); ?>">
  <div class="grid-container">
    <div class="grid-x">
      <h2 class="section-title"><?= $section_title; ?></h2>
      <h2 class="section-title-mobile"><?= $section_title_mobile; ?></h2>
      <div class="cell small-12 medium-7 large-7">
        <?php if (!is_front_page()): ?>
          <div class="<?= (is_front_page() ? 'textbox' : 'subpage-textbox'); ?>">
            <h6 class="contact-title"><?= $title; ?></h6>
            <?= $copy; ?>
            <?php if (is_front_page()): ?>
              <div class="form-holder">
                <?php echo do_shortcode('[gravityform id='.$form_id.' title=false description=false ajax=true]'); ?>
              </div> <!-- .form-holder -->
            <?php endif; ?>
          </div> <!-- .textbox -->
        <?php endif; ?>
      </div> <!-- .col-lg-6 -->

      <div class="cell small-12 medium-12 large-5 textbox-right">
        <?php if (!is_front_page()): ?>
          <div class="form-holder">
            <?php echo do_shortcode('[gravityform id='.$form_id.' title=false description=false ajax=true]'); ?>
          </div> <!-- .form-holder -->
        <?php else: ?>
          <div class="textbox">
            <h6><?= $title; ?></h6>
            <?= $copy; ?>
            <div class="decorative">
              <div class="line" data-aos="fade-down" data-aos-delay="300"></div>
              <div class="circle" data-aos="fade-in" data-aos-delay="300"></div>
            </div> <!-- .decorative -->
          </div> <!-- .textbox -->
        <?php endif; ?>
    </div> <!-- .row -->
  </div> <!-- .container-fluid -->
</section>