<?php

query_posts(array( 
  'post_type' => 'locations',
));

$terms = get_categories('locations'); 
$count = 1; ?>

<section id="section<?= get_row_index(); ?>" class="padded section google-map<?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>">
  <div class="grid-container full">
    <div class="grid-x">
      <div class="cell">

        <div class="grid-container">
          <div class="grid-x text-center align-middle">
            <div id="tabs" data-aos="fade-down" data-aos-delay="300">
              <ul class="tabs-list">
                <li class="tab tab1 active">
                  <h5><a href="#" data-filter="*">All</a></h5>
                </li>
                <?php foreach ($terms as $tab): $term_array[] = $tab; ?>
                  <?php if ($tab->slug != 'home'): ?>
                    <li class="tab tab<?= $count; ?>">
                      <h5><a href="#" data-filter="<?= strtolower($tab->slug); ?>"><?= $tab->name; ?></a></h5>
                    </li>
                  <?php endif; ?>
                <?php $count++; endforeach; ?>
              </ul> <!-- .tabs-list -->
            </div> <!-- #tabs -->
          </div> <!-- .grid-x -->
        </div> <!-- .grid-container -->

        <div class="mapHolder">
          <div id="locations-map" style="width:100%; height:800px">
            <?php 
              if (have_posts()): while (have_posts()): the_post(); 
                if (have_rows('location_info')): while(have_rows('location_info')): the_row();
                  $lat = get_sub_field('latitude'); 
                  $lng = get_sub_field('longitude'); 
                  $marker = get_sub_field('marker'); 
                  $address1 = get_sub_field('street_address_1'); 
                  $address2 = get_sub_field('street_address_2'); 
                  $city = get_sub_field('city'); 
                  $state = get_sub_field('state'); 
                  $zip_code = get_sub_field('zip_code');
                  $post_id = get_the_ID();
                  $category_object = get_the_category($post_id);
                  $category_name = $category_object[0]->slug; 

                  if ($category_name == 'home'): ?>
                    <div class="markerLogo" data-lat="<?= $lat; ?>" data-lng="<?= $lng; ?>" data-marker="<?= $marker; ?>">
                  <?php else: ?>
                    <div class="marker" data-lat="<?= $lat; ?>" data-lng="<?= $lng; ?>" data-marker="<?= $marker; ?>" data-category="<?= $category_name; ?>">
                  <?php endif; ?>
                    <strong><?php the_title(); ?></strong><br/>
                    <?php if (!empty($address1)): ?>
                      <span class="address"><?= $address1; ?></span><br/>
                    <?php endif; ?>
                    <?php if (!empty($address2)): ?>
                      <span class="address2"><?= $address2; ?></span><br/>
                    <?php endif; ?>
                    <span class="city"><?= $city; ?></span>, <span class="state"><?= $state; ?></span> <span class="zip_code"><?= $zip_code; ?></span>
                    </p>
                  </div> <!-- .marker -->
                <?php endwhile; endif; ?>
            <?php endwhile; endif; wp_reset_query(); ?>
          </div> <!-- .locations-map -->
        </div> <!-- .mapHolder -->
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section>


