<?php 
  
  $title = get_sub_field('title');
  $copy = get_sub_field('copy');
  $image1 = get_sub_field('image_1');
  $image2 = get_sub_field('image_2');
  $align_middle = get_sub_field('align_middle'); ?>

<section id="section<?= get_row_index(); ?>" class="section grid-container images-and-copy<?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>">
<!-- data-aos="fade-left" data-aos-delay="300" -->
  <div class="grid-x <?= ($align_middle==1 ? 'align-middle' : ''); ?>">
    <div class="cell small-12 medium-6 large-6 img-container img-left">
      <div class="bg" style="<?= (!empty($image2['url']) ? 'background:url('.$image2['url'].') center center / cover no-repeat' : ''); ?>"></div>
      <img class="rellax" data-rellax-speed="1" src="<?= $image1['url']; ?>" width="585" height="400" alt="neighborhood" />
    </div> <!-- .col -->
    <div class="cell small-12 medium-6 large-6 justify-content-start textbox-container textbox-right">
      <div class="textbox">
      <h6><?= $title; ?></h6>
        <p><?= $copy; ?></p>
        <div class="decorative">
          <div class="line" data-aos="fade-down" data-aos-delay="300"></div>
          <div class="circle" data-aos="fade-in" data-aos-delay="300"></div>
        </div> <!-- .decorative -->
      </div> <!-- .textbox -->
    </div> <!-- .col -->
  </div>
</section>
