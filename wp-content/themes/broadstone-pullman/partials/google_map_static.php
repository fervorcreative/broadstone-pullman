<?php if (is_front_page()): ?>
  <section id="section<?= get_row_index(); ?>" class="section grid-container full google-static-map<?php padding_top_classes(); ?><?php padding_bottom_classes(); ?>" data-aos="fade-right" data-aos-delay="300">
    <div id="map"></div>
  </section>
<?php endif; ?>