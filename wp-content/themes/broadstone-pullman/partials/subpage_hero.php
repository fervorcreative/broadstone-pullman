<?php

$hero_image = get_sub_field('hero_image');
$logo = get_sub_field('logo');
$logo_text_1 = get_sub_field('logo_text_1');
$logo_text_2 = get_sub_field('logo_text_2');
$logo_tagline = get_sub_field('logo_tagline');
$banner_headline = get_sub_field('banner_headline');
$custom_label = get_sub_field('custom_label'); ?>

<section id="section<?= get_row_index(); ?>" class="section grid-container full subpage-hero" data-aos="fade-up" data-aos-delay="300" style="<?= (!empty($hero_image['url']) ? 'background:url('.$hero_image['url'].') center center / cover no-repeat' : ''); ?>">
  <div class="grid-x row" data-aos="fade-up" data-aos-delay="500">
    <div class="cell">
      <!--<img src="<?= $logo['url']; ?>" alt="broadstone-pullman-logo" />-->
      
      <p class="tagline animated fadeIn4"><?= $logo_tagline; ?></p>
      <p class="headline"><?= $banner_headline; ?></p>
    </div> <!-- .cell -->
  </div> <!-- .grid-x -->
</section>