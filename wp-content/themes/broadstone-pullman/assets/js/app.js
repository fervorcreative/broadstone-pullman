/* global AOS, AppData, google, Rellax, lottie  */

var App = App || {},
  $ = $ || jQuery;

App.animateOnScroll = function() {
  AOS.init({
    easing: 'ease',
    duration: 800,
    disable: 'mobile'
  });
};

App.createLogoAnimation = function() {
  setTimeout(function() {
    var anim;
    var elem = document.getElementById('bodymovin');
    var animData = {
        container: elem,
        renderer: 'svg',
        loop: false,
        autoplay: true,
        rendererSettings: {
            progressiveLoad:true
        },
        path: AppData.home_url + '/wp-content/themes/broadstone-pullman/animations/logo.json'
    };
    anim = bodymovin.loadAnimation(animData);
  }, 800);
}

App.toggleActiveTabs = function() {
  $('#tabs a').on('click', function(e) {
      e.preventDefault();
      $('#tabs li').removeClass('active');
      $(this).parents('.tab').addClass('active');
  });
}

App.addButtonLinks = function() {
  $('button').on('click', function() {
    var link = $(this).data('href');
    location.href=link;
  });
}

// Google Map
App.googleMap = function() {
  function initialize() {
    var lat = 33.1506234;
    var lng = -96.8333098;
    var mapOptions = {
      zoom: 15,
      scrollwheel: false,
      saturation: -100,
      center: new google.maps.LatLng(lat, lng),
      disableDefaultUI: true,
      gestureHandling: 'none',
      styles: [
        {
            'featureType': 'administrative',
            'elementType': 'labels.text.fill',
            'stylers': [
                {
                    'color': '#444444'
                }
            ]
        },
        {
            'featureType': 'landscape',
            'elementType': 'all',
            'stylers': [
                {
                    'color': '#f2f2f2'
                }
            ]
        },
        {
            'featureType': 'poi',
            'elementType': 'all',
            'stylers': [{
              'saturation': -100
            }]
        },
        {
            'featureType': 'poi',
            'elementType': 'labels',
            'stylers': [
                {
                    'visibility': 'off',
                }
            ]
        },
        {
            'featureType': 'road',
            'elementType': 'all',
            'stylers': [
                {
                    'saturation': -100
                },
                {
                    'lightness': 45
                }
            ]
        },
        {
            'featureType': 'road.highway',
            'elementType': 'all',
            'stylers': [
                {
                    'visibility': 'simplified'
                }
            ]
        },
        {
            'featureType': 'road.highway',
            'elementType': 'geometry.fill',
            'stylers': [
                {
                    'color': '#ffffff'
                }
            ]
        },
        {
            'featureType': 'road.arterial',
            'elementType': 'labels.icon',
            'stylers': [
                {
                    'visibility': 'off'
                }
            ]
        },
        {
            'featureType': 'transit',
            'elementType': 'all',
            'stylers': [
                {
                    'visibility': 'off'
                }
            ]
        },
        {
            'featureType': 'water',
            'elementType': 'all',
            'stylers': [
                {
                    'color': '#dde6e8'
                },
                {
                    'visibility': 'on'
                }
            ]
        }
      ]
    }

    var map = new google.maps.Map(document.getElementById('map'), mapOptions);
    var image = {url: AppData.template_dir + '/assets/images/broadstone-icon.png'};
    var myLatLng = new google.maps.LatLng(lat,lng);
    var stetsonMarker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: image,
        url:  'https://goo.gl/maps/TKbcT8E3eQGHGaFd9'
    });

    google.maps.event.addListener(stetsonMarker, 'click', function() {
        window.open(stetsonMarker.url, '_blank');
    });
  }

  google.maps.event.addDomListener(window, 'load', initialize);
} //App.googleMap

// Google Map Interactive
App.googleMapInteractive = function() {
  function render_map( $el ) {
    var $markers = $el.find('.marker');
    var $markersLogo = $el.find('.markerLogo');
    var args = {
      zoom : 15,
      center: new google.maps.LatLng(0,0),
      mapTypeId : google.maps.MapTypeId.ROADMAP,
      styles: [
      {
          'featureType': 'administrative',
          'elementType': 'labels.text.fill',
          'stylers': [
              {
                  'color': '#444444'
              }
          ]
      },
      {
          'featureType': 'landscape',
          'elementType': 'all',
          'stylers': [
              {
                  'color': '#f2f2f2'
              }
          ]
      },
      {
          'featureType': 'poi',
          'elementType': 'all',
          'stylers': [
              {
                  'visibility': 'off'
              }
          ]
      },
      {
          'featureType': 'road',
          'elementType': 'all',
          'stylers': [
              {
                  'saturation': -100
              },
              {
                  'lightness': 45
              }
          ]
      },
      {
          'featureType': 'road.highway',
          'elementType': 'all',
          'stylers': [
              {
                  'visibility': 'simplified'
              }
          ]
      },
      {
          'featureType': 'road.highway',
          'elementType': 'geometry.fill',
          'stylers': [
              {
                  'color': '#ffffff'
              }
          ]
      },
      {
          'featureType': 'road.arterial',
          'elementType': 'labels.icon',
          'stylers': [
              {
                  'visibility': 'off'
              }
          ]
      },
      {
          'featureType': 'transit',
          'elementType': 'all',
          'stylers': [
              {
                  'visibility': 'off'
              }
          ]
      },
      {
          'featureType': 'water',
          'elementType': 'all',
          'stylers': [
              {
                  'color': '#dde6e8'
              },
              {
                  'visibility': 'on'
              }
          ]
      }
      ]
    }
            
    var map = new google.maps.Map( $el[0], args);
    
    map.markers = [];
    map.markersLogo = [];
    
    $markers.each(function(){
      add_marker( $(this), map );
    });

    $markersLogo.each(function() {
      add_marker_logo( $(this), map );
    })

    center_map( map );
    
    return map;

  } //render_map

  var infowindow = new google.maps.InfoWindow({
      content : '' 
  });

  function add_marker( $marker, map ) {
      var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

      var icon = {
      url: $marker.attr('data-marker'),
        //scaledSize: new google.maps.Size(28,40),
      }

      var marker = new google.maps.Marker({
          position : latlng,
          map	: map,
          icon : icon,          
          zIndex: 2
      });
      
      map.markers.push( marker );

      if( $marker.html() ) {

      $('#tabs a').on('click', function(e) {
          e.preventDefault();
          //console.log($(this).data('filter'));
          if ($(this).data('filter') == $marker.data('category')) {
            show($marker.attr('data-category'));  
           
          } else if ($(this).data('filter') != $marker.data('category') && $(this).data('filter') != '*') {
            hide($marker.attr('data-category'));
          } else if ($(this).data('filter') == '*') {
            showAllMarkers();
          }
      });

      // Show all markers
      function showAllMarkers() {
          infowindow.close( map, marker );
          marker.setVisible(true);
      }

      // Hide unfiltered markers
      function hide(category) {
          infowindow.close( map, marker );
          marker.setVisible(false);
      }

      // Show filtered markers
      function show(category) {
          infowindow.close( map, marker );
          marker.setVisible(true);
      }

      // show info window when marker is clicked & close other markers
      google.maps.event.addListener(marker, 'click', function() {
          infowindow.setContent($marker.html());
          infowindow.open(map, marker);
      });
              
      google.maps.event.addListener(map, 'click', function(event) {
          if (infowindow) {
              infowindow.close(); 
              }
      }); 
      }
  } //add_marker

  function add_marker_logo( $markerLogo, map ) {

    var image = {
      url: AppData.home_url + '/wp-content/uploads/2019/11/pullman-black.png',
    }
    
    var lat = 33.1506234;
    var lng = -96.8333098;

    var markerLogo = new google.maps.Marker({
        position: new google.maps.LatLng(lat,lng),
        map: map,
        icon: image,
        zIndex: 1,
        url:  'https://goo.gl/maps/TKbcT8E3eQGHGaFd9'
    });

    google.maps.event.addListener(markerLogo, 'click', function() {
        window.open(markerLogo.url, '_blank');
    });
    
    map.markersLogo.push( markerLogo );
  }

  function center_map( map ) {
      // vars
      var bounds = new google.maps.LatLngBounds();

      //loop through all markers and create bounds
      $.each( map.markers, function( i, marker ){
        var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
        bounds.extend( latlng );
      });

      map.setCenter( bounds.getCenter() );
      map.setCenter(new google.maps.LatLng(33.1506234,-96.8333098));
      map.setZoom(14);

  } //function center_map

  $(document).ready(function(){
    render_map($('#locations-map'));
  });  
}

App.navToggle = function() {
  $(document).on('click', '.js-nav-toggle', function() {
      if ( $('html').hasClass('is-open') ) {
        $('.nav-contain').fadeToggle(200);
        //$('.header').css('z-index', 1);
        $('.hero-content').css('z-index', 9999);
        $('.logo').css({'z-index': 9999 });
        $('.header__nav_offcanvas').css('visibility', 'hidden');
        $('html').toggleClass('is-open');
      } else {
        $('.nav-contain').fadeToggle(250);
        $('.header').css('z-index', 10000);
        $('.hero-content').css('z-index', 1);
        $('.logo').css({'z-index': 10001, 'visibility': 'visible'});
        $('.header__nav_offcanvas').css('visibility', 'visible');
          window.setTimeout( function() {
            $('html').toggleClass('is-open');
          }, 0 );
      }
});
} //function

App.imageSlider = function () {
  var $imgslider = $('.image-slider'),
    currentSlide,
    slideCount;
      
  $imgslider.each(function () {
    var $this = $(this);
    var slider_index = $this.data('index');
    //console.log(slider_index);
    var slideCounter = $('.slide-controls-' + slider_index + ' .slide-counter' );
    var updateSlideCounter = function (slick) {
        currentSlide = slick.slickCurrentSlide() + 1;
        slideCount = slick.slideCount;
        //console.log(slideCount); 
        slideCounter.html('<span class="slide-num">' + currentSlide + '</span> / <span class="slide-count">' + slideCount + '</span>');
    };

    $this.on('init', function (event, slick) {
        updateSlideCounter(slick);
    });
    
    $this.on('afterChange', function (event, slick, currentSlide) {
      updateSlideCounter(slick, currentSlide); 
    });

    $this.slick({
      dots: false,
      adaptiveHeight: true,
      appendArrows: $(this).next('.slide-controls'),
      prevArrow: '<a href="#" class="slick-prev prev-arrow"><div class="dashicons dashicons-arrow-left-alt"></div>',
      nextArrow: '<a href="#" class="slick-next next-arrow"><div class="dashicons dashicons-arrow-right-alt"></div>',
    });
  });

  $('.photos a').on('click', function (e) { 
    e.preventDefault();
    var getIndex = $(this).data('slide');
    var this_slider = $(this).parent().data('slider');
    //console.log(this_slider);
    $('#image-'+this_slider).slick('slickGoTo', getIndex - 1);
    $('html,body').stop().animate({
      'scrollTop': $('#image-'+this_slider).offset().top
    }, 400, 'swing');
    updateSlideCounter(slick, getIndex);
  });

  $('.slider-tabs a').on('click', function (e) { 
    e.preventDefault();
    
    $('html,body').stop().animate({
        'scrollTop': $('.gallery-slider').offset().top
    }, 400, 'swing');
  });
  
  $('.tabs li a').not('.videos').on('click', function (e) {
      e.preventDefault();
      $('.tabs li, .tab-wrapper .tab-content').removeClass('current-tab');
      $(this).parent().addClass('current-tab');
      
      var currentTab = $(this).attr('href');
      //console.log(currentTab);
      $(currentTab).addClass('current-tab');
      $(currentTab + '-pager').addClass('current-tab');
      
      $imgslider.slick('setPosition', 0);
  });

  $('.tabs li .videos').on('click', function (e) {
    e.preventDefault();
    $('.tabs li, .tab-wrapper .tab-pager').removeClass('current-tab');
    $(this).parent().addClass('current-tab');
    
    var currentTab = $(this).attr('href');
    //console.log(currentTab);
    $(currentTab).addClass('current-tab');
    $(currentTab + '-pager').addClass('current-tab');
    
    //$imgslider.slick('setPosition', 0);
});
}

// Display sticky header if page load is below bottom of hero

App.stickyHeaderOnload = function() {
  $(document).ready(function() {
    if ($('body').hasClass('home')) {
      var distance = $('#section2').offset().top - 300;
    } else {
      var distance = $('#section1').offset().top;
    }
    var $window = $(window);
    if ($window.scrollTop() >= distance) {
      console.log($window.scrollTop());
      $('.header').addClass('fixed-header').removeClass('static-header');
      if ($('body').hasClass('home')) {
        $('main').css('margin-top', '0px');
      } else {
        $('main').css('padding-top', '130px');
      }
      $('.home .logo').addClass('visible');
    } else if ($window.scrollTop() <= distance) {
      $('.header').removeClass('fixed-header');
      if ($('body').hasClass('home')) {
        $('main').css('margin-top', '-130px');
      } else {
        $('main').css('padding-top', '0px');
      }
      $('.home .logo').removeClass('visible');
    } //endif
  });
} //function

// Display sticky header when on scroll, when reaching bottom of hero

App.stickyHeaderScroll = function() {
  $(document).ready(function() {
    $(window).scroll(function() {
      if ($('body').hasClass('home')) {
        var distance = $('#section2').offset().top - 300;
      } else {
        var distance = $('#section1').offset().top;
      }
      var $window = $(window);
      if ($window.scrollTop() >= distance) {
        console.log($window.scrollTop());
        $('.header').addClass('fixed-header').removeClass('static-header');
        if ($('body').hasClass('home')) {
          $('main').css('margin-top', '0px');
        } else {
          $('main').css('padding-top', '130px');
        }
        $('.home .logo').addClass('visible');
      } else if ($window.scrollTop() <= distance) {
        $('.header').removeClass('fixed-header');
        if ($('body').hasClass('home')) {
          $('main').css('margin-top', '-130px');
        } else {
          $('main').css('padding-top', '0px');
        }
        $('.home .logo').removeClass('visible');
      } //endif
    });
  });
} //function

App.stickyNav = function() {
  // Create a clone of the menu, right next to original
  $('.header').addClass('original').clone().insertAfter('.header').addClass('cloned').css('position','fixed').css('top','0').css('margin-top','0').css('z-index','10000').removeClass('original').hide();

  scrollIntervalID = setInterval(stickIt, 10);

  function stickIt() {
  
  var orgElementPos = $('.original').offset();
    orgElementTop = orgElementPos.top;               

    if ($(window).scrollTop() >= (orgElementTop)  ) {
      // scrolled past the original position; now only show the cloned, sticky element.

      // Cloned element should always have same left position and width as original element.     
      orgElement = $('.original');
      coordsOrgElement = orgElement.offset();
      leftOrgElement = coordsOrgElement.left;  
      widthOrgElement = orgElement.css('width');
      $('.cloned').css('left',leftOrgElement+'px').css('top',0).css('width',widthOrgElement).show();
      $('.original').css('visibility','hidden');
    } else {
      // not scrolled past the menu; only show the original menu.
      $('.cloned').hide();
      $('.original').css('visibility','visible');
    }
  }
}

App.promoClose = function() {
  $('.promo__close').on('click', function() {
    $('#promo, .promo__close').slideUp(250, function() {
      $('header').css('top', '0');
    });
    $('#logo').removeClass('hidden');
  });
}

App.promoLink = function() {
  $('#promo-bar').on('click', function(e) {
    if($(e.target).is('.promo__close, .promo-link, .promo-link__outer, .promo-link__icon')) {
      return;
    }
    var url = '/contact/';
    location.href=url;
  });
}

App.expirePromo = function() {
  var timeInHours = .25
  var expirationDuration = 1000 * 60 * 60 * timeInHours;
  var prevAccepted = localStorage.getItem('promoShown');
  var currentTime = new Date().getTime();
  var notAccepted = prevAccepted === undefined;
  var prevAcceptedExpired = prevAccepted !== undefined && currentTime - prevAccepted > expirationDuration;
  $('#promo-bar').show();
  if (notAccepted || prevAcceptedExpired) {
    localStorage.setItem('promoShown', currentTime);
  } else {
    //$('#promo-bar').hide();
  }
} //function

// Instantiate Rellax parallax effects
App.rellaxInit = function() {
  new Rellax('.rellax', {
    speed: 2,
    center: true,
    wrapper: null,
    round: true
  });
}
App.imageLightbox = function () {
  $('.js-zoom').magnificPopup({
    type: 'image'
  });
}
// App.homePopup = function() {
//   $('.heroTab').magnificPopup({
//     items: [
//       {
//         src: '#ctaForm', // CSS selector of an element on page that should be used as a popup
//         type: 'inline'
//       }
//     ],
//   });
// }

App.expirePromo = function() {
  var timeInHours = 24
  var expirationDuration = 1000 * 60 * 60 * timeInHours;
  var prevAccepted = localStorage.getItem('promoShown');
  var currentTime = new Date().getTime();
  var notAccepted = prevAccepted === undefined;
  var prevAcceptedExpired = prevAccepted !== undefined && currentTime - prevAccepted > expirationDuration;

  if (notAccepted || prevAcceptedExpired) {
    localStorage.setItem('promoShown', currentTime);

    if ($('body').hasClass('home')) {
      setTimeout(function() {
        $.magnificPopup.open({
          items: {
              src: '#ctaForm'
          },
          type: 'inline'
  
        }, 0);
      }, 6000);
    } else {
      $.magnificPopup.open({
        items: {
            src: '#ctaForm'
        },
        type: 'inline'
  
      }, 0);
  
    }
    
  } else {
    $.magnificPopup.close({
      items: {
          src: '#ctaForm'
      },
      type: 'inline'
  
    }, 0);
  }
} //function

App.homePopupTest = function() {
  $('.heroTab').magnificPopup ({
      items: {
          src: '#ctaForm'
      },
      type: 'inline'

  });

  $('.popup-vimeo').magnificPopup({
    type: 'iframe'
  });
}


App.floorplanBlocks = function () {
  var appID = '810e546d-bac9-44b5-a25b-f3da17abad80',
    apiKey = 'ToEL2ULCH9nWn5ZrP8vsQFt7khMMAwgs';
  var filter = $('.fp-filter');
  var showData = $('.floor-plan-units');
  var filterItems = '';
  var content = '';
  
  var unitData = [];
  var bedOptions = new Array();
  var floorplanData = [];
  var finalData = [];
  // sightmap floor plan IDs don't match secure Cafe's IDs for floor plans
  // needed to hard code the relationships to provide a working link
  var link_ids = {
    57733 : 3266320,
    57715 : 3266320,
    57707 : 3266320,
    57719 : 3266320,
    57688 : 3266320,
    57689 : 3266320,
    57714 : 3266320,
    57712 : 3266320,
    57713 : 3266320,
    57706 : 3266382,
    57687 : 3266403,
    57737 : 3266444,
    57717 : 3266444,
    57711 : 3266493,
    57695 : 3266505,
    57701 : 3266528,
    57708 : 3266528,
    57693 : 3266528,
    57705 : 3266611,
    57723 : 3266615,
    57698 : 3266615,
    57724 : 3266615,
    57727 : 3266615,
    57729 : 3266615,
    57726 : 3266615,
    57694 : 3266615,
    57697 : 3266615,
    57696 : 3266615,
    57703 : 3266636,
    57692 : 3266709,
    57710 : 3266718,
    57741 : 3266719,
    57716 : 3266720,
    57718 : 3266810,
    57699 : 3266830,
    57734 : 3266830,
    57720 : 3266830,
    57690 : 3266830,
    57722 : 3266907,
    57700 : 3266907,
    57725 : 3266907,
    57738 : 3266907,
    57730 : 3266907,
    57739 : 3266907,
    57735 : 3266913,
    57709 : 3266913,
    57731 : 3266916,
    57702 : 3266916,
    57728 : 3266918,
    57736 : 3266920,
    57721 : 3266922,
    57691 : 3266922,
    57732 : 3266923,
    57704 : 3266923,
    57740 : 3266923
  }
  var studyPlans = ['A5', 'A8', 'A10','A11', 'A13'];
  
  $.when(returnUnitData(), returnFloorPlanData()).done(function (uData, fpData) {
    
    if (uData[1] == 'success' && fpData[1] == 'success') {
      var fp_data = fpData[0].data;
      var unit_data = uData[0].data;
      
      /* testing data */
      for (var uu = 0; uu < unit_data.length; uu++) {
        var testU = unit_data[uu];
        console.log(testU);
      }
      /* *** */
      
      // floor plan data 
      for (var f = 0; f < fp_data.length; f++) {
        var fItem = fp_data[f];
        //console.log(fItem);
        if (fItem.name.indexOf('.') == -1) {
          var off_name = fItem.name;
        }
        var slugName;
      
        if( fItem.bedroom_count == 0 ) {
          slugName = 'studio';
        }
        if(fItem.bedroom_count == 1 ) {
          slugName = 'one-bedroom';
        }
        if( fItem.bedroom_count == 2 ) {
          slugName = 'two-bedrooms';
        }
        if (fItem.name.indexOf('.') == -1) {
          finalData.push({
            id: fItem.id,
            link_id: link_ids[fItem.id],
            name: fItem.name,
            beds: fItem.bedroom_count,
            baths: fItem.bathroom_count,
            bedtext: (fItem.bedroom_count > 1) ? 'Bedrooms' : 'Bedroom',
            bathtext: (fItem.bathroom_count > 1) ? 'Bathrooms' : 'Bathroom',
            slug: slugName,
            image_url: fItem.image_url,
            sqfeet: get_sq_feet(fItem.id)
          });
        }
        // store bed options
        if ($.inArray(fp_data[f].bedroom_count, bedOptions) == -1) {
          bedOptions.push(fp_data[f].bedroom_count);
        }
      }
      
      function get_sq_feet(id) {
        var sqftArr = [];
        var counter = 0;
        for (var u = 0; u < unit_data.length; u++) {
          var uItem = unit_data[u];
          //console.log(uItem);
          if (id == uItem.floor_plan_id) {
            sqftArr.push(uItem.area);
            //counter++;
          }
        }
        
        //return counter + ' Found';
        if (sqftArr.length) {
          var unique_values = [];
          $.each(sqftArr, function(i, el){
            if($.inArray(el, unique_values) === -1) {unique_values.push(el);}
          });
          return unique_values.join(', ');
        }
      }
    }
    
    finalData.sort(function (a, b) {
      var x = a.name;
      var y = b.name;
      // return x < y ? -1 : x > y ? 1 : 0;
      return x.localeCompare(y, 'en', { numeric: true });
    });
    
    console.log(finalData);
    
    if (finalData.length) {
      //console.log(cleaned_data);
      showData.empty();
      
      for (var s = 0; s < finalData.length; s++) {
        
        var oItem = finalData[s];
        //var each_fp = fItem.floor_data;
        //console.log(each_fp.baths);
        // for (var z = 0; z < each_fp.length; z++) {
        //   var el = fItem.floor_data[z];
        //   var f_baths = el.baths;
        //   var f_bathtext = el.bathtext;
        //   var f_beds = el.beds;
        //   var f_bedtext = el.bedtext;
        //   var f_image_url = el.image_url;
        //   var f_link_id = el.link_id;
        //   var f_name = el.name;
        //   var f_slug = el.slu;
        // }
        var studyText = '';
        if ( studyPlans.includes(oItem.name) ) {
          studyText = '<span class="meta-insert">with Study</span>';
        }
        var sqft_text;
        if (oItem.sqfeet == undefined) {
          sqft_text = '';
        } else {
          sqft_text = oItem.sqfeet + ' sq ft';
        }
        
        content += '<div class="fp-listing mix ' + oItem.slug + '"><div class="fp-listing--image"><a href="' + oItem.image_url + '" class="js-zoom" title="Floor Plan: ' + oItem.name + ' '+ studyText.replace(/(<([^>]+)>)/gi, '') +'"><img src="' + oItem.image_url + '" alt="' + oItem.name + '" width="400"></a></div>';
          content += '<div class="fp-listing--details"><div class="fp-listing--content">';
          content += '<h3 class="fp-listing--title">' + oItem.name  + ' ' + studyText + '</h3>';
          content += '<p class="fp-listing--info">';
          if (oItem.beds > 0 && oItem.baths > 0 ) {
            content += oItem.beds + ' ' + oItem.bedtext + ' | ' + oItem.baths + ' ' + oItem.bathtext + '<br>' + oItem.sqfeet + ' sq ft';
          } else {
            content += 'Studio | 1 Bath | ' + oItem.sqfeet + ' sq ft';
          }
          //content += f_sqft + ' sq ft';
          content += '</p>';
          content += '<p class="fp-listing--info"><a href="https://broadstonepullman.securecafe.com/onlineleasing/broadstone-pullman/oleapplication.aspx?stepname=Apartments&myOlePropertyId=1120310&floorPlans=' + oItem.link_id + '&MoveInDate=" target="_blank">View Availability</a></p>';
          content += '</div></div></div>';
      }
      showData.html(content);
      
      /*
      returns this...
      0 => 1
      1 => 2
      3 => 0
      */
      //filter += '<li><a href="javascript:;" title="All" data-filter="all">All</a></li>';
      bedOptions.sort(function(a, b){return a-b});
      for( var j=0; j<bedOptions.length; j++ ) {
        var unitName = '',
          slugName = '';

        if( bedOptions[j] == 0 ) {
          unitName = 'Studio';
          slugName = 'studio';
        }
        if(bedOptions[j] == 1 ) {
          unitName = '1 Bedroom';
          slugName = 'one-bedroom';
        }
        if( bedOptions[j] == 2 ) {
          unitName = '2 Bedrooms';
          slugName = 'two-bedrooms';
        }
        // if( bedOptions[j] == 3 ) {
        //   unitName = '3 Bedrooms';
        //   slugName = 'three-bedrooms';
        // }
        filterItems += '<li><a href="javascript:;" title="'+ unitName +'" data-filter=".'+ slugName +'">'+ unitName +'</a></li>';
      }
      filter.append(filterItems);
      
      $('.fp-filter li a').on('click', function (e) {
        e.preventDefault();
        var target = $(this).data('filter');
        $('.fp-filter li, .floor-plan-units div').removeClass('is-active');
        $(this).parent().addClass('is-active');
        if (target != 'all') {
          $(target).addClass('is-active');
          if (!$('.floor-plan-units').hasClass('active-elements')) {
            $('.floor-plan-units').addClass('active-elements');
          }
        } else {
          $('.floor-plan-units').removeClass('active-elements');
          $('.floor-plan-units div').removeClass('is-active');
          
        }
        
      });
      
      $('.fp-listing--image a.js-zoom').on('click', function (e) {
        e.preventDefault();
        var target = $(this).attr('href');
        var img_caption = $(this).attr('title');
        $.magnificPopup.open({
          image: {
            markup: '<div class="mfp-figure custom-figure">'+
                      '<div class="mfp-close"></div>'+
                      '<div class="mfp-img"></div>'+
                      '<div class="mfp-bottom-bar">'+
                        '<div class="mfp-title"></div>'+
                        '<div class="mfp-counter"></div>'+
                      '</div>'+
                    '</div>',
            titleSrc: function () { 
              return img_caption;
            },
            verticalFit: true, // Fits image in area vertically
            tError: '<a href="%url%">The image</a> could not be loaded.' // Error message
          },
          items: {
            src: target
          },
          type: 'image'
        }, 0);
      });
      $.magnificPopup.close();
      
    } else {
      showData.html('<p style="color: white">No floor plans to show.</p>');
    }
  });
  
  
  function sortObj(obj) {
    return Object.keys(obj).sort().reduce(function (result, key) {
      result[key] = obj[key];
      return result;
    }, {});
  }
  
  function returnUnitData() {
    return $.ajax({
      url: 'https://api.sightmap.com/v1/assets/4104/multifamily/units',
      headers: {
        'API-Key': apiKey
      },
      method: 'GET',
      dataType: 'json',
    });
  }
  
  function returnFloorPlanData() {
    return $.ajax({
      url: 'https://api.sightmap.com/v1/assets/4104/multifamily/floor-plans',
      headers: {
        'API-Key': apiKey
      },
      method: 'GET',
      dataType: 'json',
      beforeSend: function () {
        showData.html('<p style="color: #fff;">Loading floor plans. Please wait.</p>');
      },
      done: function () {
        console.log('I am finished');
        // $('.js-zoom').magnificPopup({
        //   type: 'image'
        // });
      }
    });
  }
  /*
  
  $.ajax({
    //url: 'https://api.sightmap.com/v1/assets/4104/multifamily/units',
    url: 'https://api.sightmap.com/v1/assets/4104/multifamily/floor-plans',
    //url: 'https://sightmap.com/app/api/v1/6m9pz4o7vk1/sightmaps/4926',
    headers: {
      'API-Key': apiKey
    },
    method: 'GET',
    dataType: 'json',
    beforeSend: function () {
      showData.html('<p style="color: #fff;">Loading floor plans. Please wait.</p>');
    },
    success: function (data) {
      showData.empty();
      //console.log('succes: ' + data);
      console.log(data);
      var plans = data.data,
        content = '',
        filterItems = '';
      
      for (var i = 0; i < plans.length; i++) {
        var fItem = plans[i];
        
        //console.log(item);
        floorplanData.push({
          fp_id: fItem.id, // floor plan ID
          name: fItem.name,
          beds: fItem.bedroom_count,
          baths: fItem.bathroom_count,
          image_url: fItem.image_url
        });
        var bedtext = (fItem.bedroom_count > 1 ) ? 'Bedrooms' : 'Bedroom';
        var bathtext = (fItem.bathroom_count > 1 ) ? 'Bathrooms' : 'Bathroom';
        
        content += '<div class="fp-listing"><div class="fp-listing--image"><a href="https://broadstonepullman.securecafe.com/onlineleasing/broadstone-pullman/oleapplication.aspx?stepname=floorplan&myOlePropertyId=1120310" target="_blank"><img src="'+ fItem.image_url +'" alt="" width="400"></a></div>';
        content += '<div class="fp-listing--details"><div class="fp-listing--content">';
        content += '<h3 class="fp-listing--title">'+ fItem.name +'</h3>';
        content += '<p class="fp-listing--info">'+ fItem.bedroom_count +' ' + bedtext + ' | '+ fItem.bathroom_count +' '+ bathtext + ' <br><a href="https://broadstonepullman.securecafe.com/onlineleasing/broadstone-pullman/oleapplication.aspx?stepname=floorplan&myOlePropertyId=1120310" target="_blank">View Availability</a></p>';
        content += '</div></div></div>';
      }
      
      showData.append(content);
      
    },
    done: function () {
      console.log('<p style="color: #fff;">All done. Set up additional javascript functionalities.</p>');
    },
    fail: function () {
      showData.html('<p style="color: #fff;">We had a problem.</p>');
    }
  });
  */
}
function dump(v) {
  switch (typeof v) {
    case 'object':
      for (var i in v) {
        console.log(i+':'+v[i]);
      }
      break;
    default: //number, string, boolean, null, undefined 
      console.log(typeof v+':'+v);
      break;
  }
}


// Instantiate AOS
$(window).on('load', function () {
  App.animateOnScroll();

  if ($('body').hasClass('home')) {
    App.googleMap();
    App.createLogoAnimation();
  }

  if ($('body').hasClass('neighborhood')) {
    App.googleMapInteractive();
  }
  App.floorplanBlocks();
  App.toggleActiveTabs();
  App.addButtonLinks();
  App.imageSlider();
  App.rellaxInit();
  App.navToggle();
  //App.promoClose();
  App.promoLink();
  //App.expirePromo();
  App.stickyNav();
  //App.homePopupTest();
  App.imageLightbox();
});