<!DOCTYPE html>

<html lang="en" class="bgFade">

<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="shortcut icon" href="<?php bloginfo('url'); ?>/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="https://use.typekit.net/fgz2fzt.css">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="site">
<div id="promo-bar">
      <div id="promo">
        <div id="promo-inner">
          <h3><a href="<?php echo esc_url( get_permalink( 912 ) ); ?>">Click here</a> for COVID-19-related information and current specials</h3>
        </div>
        <div class="promo__close">
          <div class="promo-link js-promo-toggle">
            <div class="promo-link__outer">
              <div class="promo-link__icon"></div>
            </div> <!-- .promo-link__outer -->
          </div> <!-- .promo-link -->
        </div> <!-- .promo__close -->
      </div> <!-- #promo -->
	  </div> <!-- #promo-bar -->

	<header class="header" role="banner">
    <div class="logo">
      <a href="/"><img src="/wp-content/uploads/2019/11/logo-1.png" alt="" /></a>
    </div> <!-- .logo-img -->
    <nav class="header__nav_offcanvas grid-container full">
      <div class="grid-x">
        <div class="cell large-12">
          <?php wp_nav_menu(array('theme_location' => 'header_offcanvas', 'depth' => 1)); ?>
          <div id="links">
            <!--<a class="apply" href="https://broadstonepullman.securecafe.com/onlineleasing/broadstone-pullman/guestlogin.aspx" target="_blank">Apply Now</a>-->
            <a class="phone" href="tel:833889.896" target="_blank">833.889.0896</a>
            <a class="resident-login" href="https://broadstonepullman.securecafe.com/residentservices/broadstone-pullman/userlogin.aspx" target="_blank">Resident Login</a>
            <a class="social" href="https://www.instagram.com/broadstonepullman/" target="_blank">Instagram</a> <span class="divider">|</span> <a class="social" href="https://business.facebook.com/BroadstonePullman/" target="_blank">Facebook</a> <span class="divider">|</span> <a class="social" href="https://www.youtube.com/channel/UCBs1eJz3oMx708BJdodAQfg/videos" target="_blaank">YouTube</a>
          </div> <!-- #links -->
        </div> <!-- .cell -->
      </div> <!-- .grid-x -->
    </nav>

		<div class="header__nav_oncanvas grid-container full">
			<div class="grid-x align-middle align-center">
        <div class="cell">
          <div class="header__content">
            <nav class="header__nav grid-container">
              <div class="grid-x">
                <div class="cell large-12">
                  <?php wp_nav_menu(array('theme_location' => 'header', 'depth' => 1)); ?>
                </div> <!-- .cell -->

                <div class="nav-link js-nav-toggle">
                  <div class="nav-link__outer">
                    <div class="nav-link__icon"></div>
                  </div> <!-- .nav-link -->
                </div> <!-- .nav-link -->
              </div> <!-- .grid-x -->
            </nav>
          </div> <!-- .header__content -->
        </div> <!-- .cell -->
      </div> <!-- .grid-x -->
    </div> <!-- .grid-container -->
  </header>

  <main role="main">
